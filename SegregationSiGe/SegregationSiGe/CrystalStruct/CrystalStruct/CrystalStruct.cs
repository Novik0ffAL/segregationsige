﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace SegregationSiGe.CrystalStruct.CrystalStruct
{
    public class CrystalStruct : ICreate, ISetAtomsType
    {
        public Cell.Cell[,,] Cells { get; set; }
        public Coordinate.Coordinate CrystallStructSize { get; set; }
        public double CellParameter { get; set; }
        public double ConcentrationGe { get; set; }
        public List<Atom.Atom> Atoms { get; set; }
        public void Create()
        {
            try
            {
                Cells = new Cell.Cell[(int)CrystallStructSize.X, (int)CrystallStructSize.Y, (int)CrystallStructSize.Z];
                int number = 0;
                for (int i = 0; i < Cells.GetLength(0); i++)
                    for (int j = 0; j < Cells.GetLength(1); j++)
                        for (int k = 0; k < Cells.GetLength(2); k++)
                        {
                            Cells[i, j, k] = new Cell.Cell
                            {
                                CellCoorfinate = new Coordinate.Coordinate { X = i, Y = j, Z = k },
                                Atoms = new List<Atom.Atom>(),
                                CellsNeighbours = new Dictionary<Coordinate.Coordinate, Cell.Cell>()
                            };
                            //Основная ГЦК - решетка
                            Cells[i, j, k].Atoms.Add(new Atom.Atom
                            {
                                Coordinate = new Coordinate.Coordinate { X = 0.0, Y = 0.0, Z = 0.0 },
                                CellCoordinate = Cells[i, j, k].CellCoorfinate,
                                Index = number
                            });
                            number++;
                            Cells[i, j, k].Atoms.Add(new Atom.Atom
                            {
                                Coordinate = new Coordinate.Coordinate { X = 0.0 * CellParameter, Y = 0.5 * CellParameter, Z = 0.5 * CellParameter },
                                CellCoordinate = Cells[i, j, k].CellCoorfinate,
                                Index = number
                            });
                            number++;
                            Cells[i, j, k].Atoms.Add(new Atom.Atom
                            {
                                Coordinate = new Coordinate.Coordinate { X = 0.5 * CellParameter, Y = 0.0 * CellParameter, Z = 0.5 * CellParameter },
                                CellCoordinate = Cells[i, j, k].CellCoorfinate,
                                Index = number
                            });
                            number++;
                            Cells[i, j, k].Atoms.Add(new Atom.Atom
                            {
                                Coordinate = new Coordinate.Coordinate { X = 0.5 * CellParameter, Y = 0.5 * CellParameter, Z = 0.0 * CellParameter },
                                CellCoordinate = Cells[i, j, k].CellCoorfinate,
                                Index = number
                            });
                            number++;
                            //Сдвинутая ГЦК - решетка
                            Cells[i, j, k].Atoms.Add(new Atom.Atom
                            {
                                Coordinate = new Coordinate.Coordinate { X = 0.25 * CellParameter, Y = 0.25 * CellParameter, Z = 0.25 * CellParameter },
                                CellCoordinate = Cells[i, j, k].CellCoorfinate,
                                Index = number
                            });
                            number++;
                            Cells[i, j, k].Atoms.Add(new Atom.Atom
                            {
                                Coordinate = new Coordinate.Coordinate { X = 0.25 * CellParameter, Y = 0.75 * CellParameter, Z = 0.75 * CellParameter },
                                CellCoordinate = Cells[i, j, k].CellCoorfinate,
                                Index = number
                            });
                            number++;
                            Cells[i, j, k].Atoms.Add(new Atom.Atom
                            {
                                Coordinate = new Coordinate.Coordinate { X = 0.75 * CellParameter, Y = 0.25 * CellParameter, Z = 0.75 * CellParameter },
                                CellCoordinate = Cells[i, j, k].CellCoorfinate,
                                Index = number
                            });
                            number++;
                            Cells[i, j, k].Atoms.Add(new Atom.Atom
                            {
                                Coordinate = new Coordinate.Coordinate { X = 0.75 * CellParameter, Y = 0.75 * CellParameter, Z = 0.25 * CellParameter },
                                CellCoordinate = Cells[i, j, k].CellCoorfinate,
                                Index = number
                            });
                            number++;
                        }

                Atoms = new List<Atom.Atom>();
                foreach (var cell in Cells)
                {
                    cell.CellsNeighbours = cell.GetCellNeighbour(this);
                    foreach (var atom in cell.Atoms)
                    {
                        atom.Acceleration = new Coordinate.Coordinate();
                        atom.Speed = new Coordinate.Coordinate();
                        Atoms.Add(atom);
                    }
                }
            }
            catch (Exception)
            {
                throw new ArgumentException($@"Error - Не заданы параметры структуры");
            }
        }
        public void SetAtomsType(int distribution, int countGroup = 0)
        {
            foreach (var item in Cells)
            {
                foreach (var atom in item.Atoms) { atom.GetNeighbours(this, CellParameter / 2); }
            }
            //делаем все атомы в структуре атомами - Si
            for (int i = 0; i < Atoms.Count(); i++)
            {
                Atoms[i].Type = (int)AtomType.AtomTypes.Si;
            }
            int countTemp = (int)(Atoms.Count() * ConcentrationGe); // вычисляем количество атомов Ge

            if (distribution == (int)Distribution.Distributions.Random)
            {
                Random random = new Random(); // переменная для рандома
                while (countTemp > 0)
                {
                    //Выбираем случайный атом
                    int Temp = Math.Abs(random.Next()) % Atoms.Count();
                    //проверяем если он не является атомом Ge
                    if (Atoms[Temp].Type != (int)AtomType.AtomTypes.Ge)
                    {
                        //то делаем его атомом Ge
                        Atoms[Temp].Type = (int)AtomType.AtomTypes.Ge;
                        countTemp--;
                    }
                }
            }
            if (distribution == (int)Distribution.Distributions.Segregation)
            {
                if (Atoms.Count() < countGroup)
                {
                    Console.Write("Атомов в группе не может быть больше количества атомов в структуре");
                }
                else
                {
                    //расчитываем количество узлов где в структуре будут скопления атомов Ge
                    int nodes = (int)(Atoms.Count() * ConcentrationGe / countGroup);
                    //задаем группы скоплений атомов
                    for (int i = 0; i < nodes; i++) { Segregation(countGroup); }
                    //если атомов Ge задали меньшее количество
                    if ((int)(Atoms.Count() * ConcentrationGe) > nodes * countGroup)
                    {
                        Segregation((int)(Atoms.Count() * ConcentrationGe) - nodes * countGroup);
                    }
                }
            }
        }
        private void Segregation(int countGroup)
        {
            int temp = countGroup;
            Random rnd = new Random();
            while (temp > 0)
            {
                //если случайный атом - Si
                int rndValue = Math.Abs(rnd.Next() % Atoms.Count());
                if (Atoms[rndValue].Type == (int)AtomType.AtomTypes.Si)
                {
                    //делаем найденый атом - атомом Ge
                    Atoms[rndValue].Type = (int)AtomType.AtomTypes.Ge;
                    temp--;
                    //создаем список куда будем записываать атомы группы
                    List<int> groupAtoms = new List<int>();
                    //добавили наш найденный атом - узел группы
                    groupAtoms.Add(rndValue);
                    while (temp > 0)
                    {
                        //выбираем случайный атом в группе
                        int tempAtom = Math.Abs(rnd.Next() % groupAtoms.Count());
                        //находим его случайного соседа
                        int tempNeighbourhood = Math.Abs(rnd.Next() % Atoms[groupAtoms[tempAtom]].Neighbours.Count());
                        //проверяем если он атом Si, то меняем его на атом Ge
                        if (Atoms[groupAtoms[tempAtom]].Neighbours.ElementAt(tempNeighbourhood).Key.Type == (int)AtomType.AtomTypes.Si)
                        {
                            Atoms[groupAtoms[tempAtom]].Neighbours.ElementAt(tempNeighbourhood).Key.Type = (int)AtomType.AtomTypes.Ge;
                            temp--;
                        }
                        if (!groupAtoms.Contains(Atoms[groupAtoms[tempAtom]].Neighbours.ElementAt(tempNeighbourhood).Key.Index))
                        {
                            groupAtoms.Add(Atoms[groupAtoms[tempAtom]].Neighbours.ElementAt(tempNeighbourhood).Key.Index);
                        }
                    }
                }
            }
        }
    }
}
