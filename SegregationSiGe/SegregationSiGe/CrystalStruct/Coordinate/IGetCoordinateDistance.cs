﻿namespace SegregationSiGe.CrystalStruct.Coordinate
{
    interface IGetCoordinateDistance
    {
        double GetCoordinateDistance(Coordinate coordinate);
    }
}
