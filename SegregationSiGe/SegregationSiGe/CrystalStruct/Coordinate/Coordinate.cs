﻿using System;

namespace SegregationSiGe.CrystalStruct.Coordinate
{
    public class Coordinate : IGetCoordinateDistance, IEquals
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
        public double GetCoordinateDistance(Coordinate coordinate)
        {
            return Math.Sqrt((X - coordinate.X) * (X - coordinate.X)
                           + (Y - coordinate.Y) * (Y - coordinate.Y)
                           + (Z - coordinate.Z) * (Z - coordinate.Z));
        }
        public bool Equals(Coordinate coordinate)
        {
            if (X == coordinate.X && Y == coordinate.Y && Z == coordinate.Z) return true;
            return false;
        }
    }
}
