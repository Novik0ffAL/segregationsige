﻿namespace SegregationSiGe.CrystalStruct.Coordinate
{
    interface IEquals
    {
        bool Equals(Coordinate coordinate);
    }
}
