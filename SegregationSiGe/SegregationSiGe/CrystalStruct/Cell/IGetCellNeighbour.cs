﻿using System.Collections.Generic;

namespace SegregationSiGe.CrystalStruct.Cell
{
    interface IGetCellNeighbour
    {
        Dictionary<Coordinate.Coordinate, Cell> GetCellNeighbour(CrystalStruct.CrystalStruct crystalStruct);
    }
}
