﻿namespace SegregationSiGe.CrystalStruct.Cell
{
    interface IGetCell
    {
        Cell GetCell(CrystalStruct.CrystalStruct crystalStruct, Coordinate.Coordinate coordinate);
    }
}
