﻿using System;

namespace SegregationSiGe.CrystalStruct.BondType
{
    public class BondType : IGetBondType
    {
        public enum BondTypes { GeGe, SiSi, SiGe }
        public bool GetBondType(Atom.Atom a, Atom.Atom b, out int typeBond)
        {
            if (a.Type == (int) AtomType.AtomTypes.Ge && b.Type == (int)AtomType.AtomTypes.Ge)
            {
                typeBond = (int)BondTypes.GeGe;
                return true;
            }
            if (a.Type == (int)AtomType.AtomTypes.Si && b.Type == (int)AtomType.AtomTypes.Si)
            {
                typeBond = (int)BondTypes.SiSi;
                return true;
            }
            if ((a.Type == (int)AtomType.AtomTypes.Ge && b.Type == (int)AtomType.AtomTypes.Si) ||
                (b.Type == (int)AtomType.AtomTypes.Ge && a.Type == (int)AtomType.AtomTypes.Si))
            {
                typeBond = (int)BondTypes.SiGe;
                return true;
            }
            throw new ArgumentException("Неверные типы атомов");
        }
    }
}
