﻿using System;
namespace SegregationSiGe.CrystalStruct.Atom
{
    interface IGetNeighbours
    {
        void GetNeighbours(CrystalStruct.CrystalStruct crystalStruct, double distance);
    }
}
