﻿namespace SegregationSiGe.CrystalStruct.Atom
{
    interface IGetAtomDistance
    {
        double GetAtomDistance(Atom atom);
    }
}
