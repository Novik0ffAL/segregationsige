﻿using System;
using System.Collections.Generic;

namespace SegregationSiGe.CrystalStruct.Atom
{
    public class Atom : IGetNeighbours
    {
        public int Index { get; set; }
        public int Type { get; set; }
        public Coordinate.Coordinate Coordinate { get; set; }
        public Coordinate.Coordinate Speed { get; set; }
        public Coordinate.Coordinate Acceleration { get; set; }
        public Coordinate.Coordinate CellCoordinate { get; set; }
        public Dictionary<Atom, Coordinate.Coordinate> Neighbours { get; set; }
        public Dictionary<Atom,double> NeighbourLenght { get; set; }
        public void GetNeighbours(CrystalStruct.CrystalStruct crystalStruct, double distance)
        {
            try
            {
                NeighbourLenght = new Dictionary<Atom, double>();
                Neighbours = new Dictionary<Atom, Coordinate.Coordinate>();
                foreach (var cell in crystalStruct.Cells[(int)CellCoordinate.X, (int)CellCoordinate.Y, (int)CellCoordinate.Z].CellsNeighbours)
                {
                    foreach (var atom in cell.Value.Atoms)
                    {
                        double tempDistance = Coordinate.GetCoordinateDistance(new Coordinate.Coordinate
                        {
                            X = atom.Coordinate.X + crystalStruct.CellParameter * cell.Key.X,
                            Y = atom.Coordinate.Y + crystalStruct.CellParameter * cell.Key.Y,
                            Z = atom.Coordinate.Z + crystalStruct.CellParameter * cell.Key.Z
                        });

                        if (Index != atom.Index && tempDistance < distance)
                        {
                            Neighbours.Add(atom, cell.Key);
                            NeighbourLenght.Add(atom, tempDistance);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw new ArgumentException($@"error in SiGe.CrystalStruct.Cell.GetCellNeighbour");
            }
        }
        public void CheckCell(CrystalStruct.CrystalStruct crystalStruct)
        {
            Coordinate.Coordinate temp = new Coordinate.Coordinate { X = CellCoordinate.X, Y = CellCoordinate.Y, Z = CellCoordinate.Z };

            if (Coordinate.X < 0) { Coordinate.X += crystalStruct.CellParameter; temp.X--; }
            if (Coordinate.X > crystalStruct.CellParameter) { Coordinate.X -= crystalStruct.CellParameter; temp.X++; }

            if (Coordinate.Y < 0) { Coordinate.Y += crystalStruct.CellParameter; temp.Y--; }
            if (Coordinate.Y > crystalStruct.CellParameter) { Coordinate.Y -= crystalStruct.CellParameter; temp.Y++; }

            if (Coordinate.Z < 0) { Coordinate.Z += crystalStruct.CellParameter; temp.Z--; }
            if (Coordinate.Z > crystalStruct.CellParameter) { Coordinate.Z -= crystalStruct.CellParameter; temp.Z++; }

            if (!CellCoordinate.Equals(temp))
            {
                crystalStruct.Cells[(int)CellCoordinate.X, (int)CellCoordinate.Y, (int)CellCoordinate.Z].Atoms.Remove(this);

                new Cell.Cell().GetCell(crystalStruct, temp).Atoms.Add(this);

            }

        }
    }
}
