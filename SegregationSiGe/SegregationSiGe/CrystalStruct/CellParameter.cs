﻿namespace SegregationSiGe.CrystalStruct
{
    public class CellParameter
    {
        public const double CellSiParameter = 0.543072;

        public const double CellGeParameter = 0.565748;
    }
}
