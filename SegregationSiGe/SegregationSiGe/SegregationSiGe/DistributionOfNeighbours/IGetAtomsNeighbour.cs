﻿namespace SegregationSiGe.DistributionOfNeighbours
{
    interface IGetAtomsNeighbour
    {
        void GetAtomsNeighbour(CrystalStruct.CrystalStruct.CrystalStruct crystalStruct);
    }
}
