﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SegregationSiGe.DistributionOfNeighbours
{
    class DistributionOfNeighbors : IWriteToFile, IGetAtomsNeighbour
    {
        private List<Dictionary<string, double>> AtomNeighboursList { get; } = new List<Dictionary<string, double>>();
        private List<Dictionary<string, double>> SiNeighboursList { get; } = new List<Dictionary<string, double>>();
        private List<Dictionary<string, double>> GeNeighboursList { get; } = new List<Dictionary<string, double>>();

        public void WriteToFile(string path)
        {
            File.WriteAllText($"{path}.csv", "");
            string export = "Для всех атомов\r\n";
            for (int i = 0; i < 5; i++)
            {
                export += $"{i.ToString()};";
                foreach (var item in AtomNeighboursList)
                {
                    export += item[i.ToString()] + ";";
                }
                export += "\r\n";
            }
            export += "\r\nДля атмов Si\r\n";
            for (int i = 0; i < 5; i++)
            {
                export += $"{i.ToString()};";
                foreach (var item in SiNeighboursList)
                {
                    export += item[i.ToString()] + ";";
                }
                export += "\r\n";
            }
            export += "\r\nДля атмов Ge\r\n";
            for (int i = 0; i < 5; i++)
            {
                export += $"{i.ToString()};";
                foreach (var item in GeNeighboursList)
                {
                    export += item[i.ToString()] + ";";
                }
                export += "\r\n";
            }
            File.WriteAllText($"{path}.csv", export, Encoding.GetEncoding(1251));
        }
        public void GetAtomsNeighbour(CrystalStruct.CrystalStruct.CrystalStruct crystalStruct)
        {
            AtomNeighboursList.Add(GetAllAtomsNeighbour(crystalStruct));
            SiNeighboursList.Add(GetTypeNeighbour(crystalStruct, (int)CrystalStruct.AtomType.AtomTypes.Si));
            GeNeighboursList.Add(GetTypeNeighbour(crystalStruct, (int)CrystalStruct.AtomType.AtomTypes.Ge));
        }
        private Dictionary<string, double> GetAllAtomsNeighbour(CrystalStruct.CrystalStruct.CrystalStruct Struct)
        {
            double[] mas = { 0, 0, 0, 0, 0 };

            Dictionary<string, double> AtomNeighbours = new Dictionary<string, double>();
            foreach (var item in Struct.Atoms)
            {
                switch (CalculateNeighbours(item))
                {
                    case 0: { mas[0]++; break; }
                    case 1: { mas[1]++; break; }
                    case 2: { mas[2]++; break; }
                    case 3: { mas[3]++; break; }
                    case 4: { mas[4]++; break; }
                }
            }
            for (int i = 0; i < mas.Length; i++)
            {
                mas[i] /= Struct.Atoms.Count();
            }
            AtomNeighbours.Add("0", mas[0]);
            AtomNeighbours.Add("1", mas[1]);
            AtomNeighbours.Add("2", mas[2]);
            AtomNeighbours.Add("3", mas[3]);
            AtomNeighbours.Add("4", mas[4]);

            return AtomNeighbours;
        }
        private Dictionary<string, double> GetTypeNeighbour(CrystalStruct.CrystalStruct.CrystalStruct Struct, int Type)
        {
            double[] mas = { 0, 0, 0, 0, 0 };
            Dictionary<string, double> keyValuePairs = new Dictionary<string, double>();
            double temp = 0;
            foreach (var item in Struct.Atoms)
            {
                if (item.Type == Type)
                {
                    temp++;
                    switch (CalculateNeighbours(item))
                    {
                        case 0: { mas[0]++; break; }
                        case 1: { mas[1]++; break; }
                        case 2: { mas[2]++; break; }
                        case 3: { mas[3]++; break; }
                        case 4: { mas[4]++; break; }
                    }
                }
            }
            for (int i = 0; i < mas.Length; i++)
            {
                mas[i] /= temp;
            }
            keyValuePairs.Add("0", mas[0]);
            keyValuePairs.Add("1", mas[1]);
            keyValuePairs.Add("2", mas[2]);
            keyValuePairs.Add("3", mas[3]);
            keyValuePairs.Add("4", mas[4]);

            return keyValuePairs;
        }
        private int CalculateNeighbours(CrystalStruct.Atom.Atom Atom)
        {
            int result = 0;
            foreach (var neightbour in Atom.Neighbours)
            {
                if (neightbour.Key.Type == Atom.Type)
                {
                    result++;
                }
            }
            return result;
        }
    }
}
