﻿namespace SegregationSiGe.Relaxation
{
    interface ISetRelaxation
    {
        void SetRelaxation(CrystalStruct.CrystalStruct.CrystalStruct crystalStruct, double dt, int maxSteps, double temperature, bool norm = true);
    }
}
