﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;


namespace SegregationSiGe.Relaxation
{
    class Relaxation : IWriteToFile, ISetRelaxation
    {
        private double Delta { get; set; }
        public List<double> ListEP { get; set; }
        public List<double> ListEK { get; set; }
        public List<double> ListTime { get; set; }
        public Relaxation(double Delta)
        {
            this.Delta = Delta;
            ListEP = new List<double>();
            ListEK = new List<double>();
            ListTime = new List<double> { 0.0 };
        }
        public void SetRelaxation(CrystalStruct.CrystalStruct.CrystalStruct crystalStruct, double dt, int maxSteps, double temperature, bool norm = true)
        {
            double kineticEquilibriumEnergy = 3.0 / 2.0 * crystalStruct.Atoms.Count * Energy.Constants.K * temperature;
            for (int i = 0; i < maxSteps; i++)
            {
                ListTime.Add(ListTime[ListTime.Count - 1] + dt);
                Iteration(crystalStruct, dt, i % 10 == 0 ? true : false);
                ListEP.Add(new Energy.Tersoff.Tersoff().TotalTersoffEnergy(crystalStruct));
                ListEK.Add(new Energy.KineticEnergy.KineticEnergy().TotalKineticEnergy(crystalStruct));
                if (norm){NormalizationSpeeds(crystalStruct, kineticEquilibriumEnergy);}
                Console.Write("\r\nStep - " + i + "\r\nПотенциальная энергия = " + ListEP[ListEP.Count - 1] +
                "\r\n" + "Кинетическая энергия = " + ListEK[ListEK.Count - 1]);
            }
        }
        public void WriteToFile(string path)
        {
            File.WriteAllText($"{path}.csv", "");
            for (int i = 0; i < ListEP.Count; i++)
            {
                File.AppendAllText($"{path}.csv", ListTime[i].ToString() + ";" + ListEP[i].ToString() + "; ;" + ListTime[i].ToString() + ";" + ListEK[i].ToString() + ";\r\n", Encoding.GetEncoding(1251));
            }
        }
        private void Iteration(CrystalStruct.CrystalStruct.CrystalStruct crystalStruct, double dt, bool getNeighbours)
        {
            for (int i = 0; i < crystalStruct.Atoms.Count; i++)
            {
                //запоминаем первоначальное ускорение
                CrystalStruct.Coordinate.Coordinate startAcceleration = new CrystalStruct.Coordinate.Coordinate
                {
                    X = crystalStruct.Atoms[i].Acceleration.X,
                    Y = crystalStruct.Atoms[i].Acceleration.Y,
                    Z = crystalStruct.Atoms[i].Acceleration.Z
                };
                CrystalStruct.Coordinate.Coordinate startCoordinate = new CrystalStruct.Coordinate.Coordinate
                {
                    X = crystalStruct.Atoms[i].Coordinate.X,
                    Y = crystalStruct.Atoms[i].Coordinate.Y,
                    Z = crystalStruct.Atoms[i].Coordinate.Z
                };
                CrystalStruct.Coordinate.Coordinate startSpeed = new CrystalStruct.Coordinate.Coordinate
                {
                    X = crystalStruct.Atoms[i].Speed.X,
                    Y = crystalStruct.Atoms[i].Speed.Y,
                    Z = crystalStruct.Atoms[i].Speed.Z
                };
                //задаем новые координаты
                crystalStruct.Atoms[i].Coordinate.X = startCoordinate.X + crystalStruct.Atoms[i].Speed.X * dt + 1 / (2.0 * new CrystalStruct.Mass.Mass().GetAtomMass(crystalStruct.Atoms[i].Type)) * startAcceleration.X * dt * dt;
                crystalStruct.Atoms[i].Coordinate.Y = startCoordinate.Y + crystalStruct.Atoms[i].Speed.Y * dt + 1 / (2.0 * new CrystalStruct.Mass.Mass().GetAtomMass(crystalStruct.Atoms[i].Type)) * startAcceleration.Y * dt * dt;
                crystalStruct.Atoms[i].Coordinate.Z = startCoordinate.Z + crystalStruct.Atoms[i].Speed.Z * dt + 1 / (2.0 * new CrystalStruct.Mass.Mass().GetAtomMass(crystalStruct.Atoms[i].Type)) * startAcceleration.Z * dt * dt;
                //расчитываем новое ускорение
                crystalStruct.Atoms[i].Acceleration = GetAcceleration(crystalStruct, i, getNeighbours);
                //задаем новые скорости
                crystalStruct.Atoms[i].Speed.X += dt * (startAcceleration.X + crystalStruct.Atoms[i].Acceleration.X) / (2.0 * new CrystalStruct.Mass.Mass().GetAtomMass(crystalStruct.Atoms[i].Type));
                crystalStruct.Atoms[i].Speed.Y += dt * (startAcceleration.Y + crystalStruct.Atoms[i].Acceleration.Y) / (2.0 * new CrystalStruct.Mass.Mass().GetAtomMass(crystalStruct.Atoms[i].Type));
                crystalStruct.Atoms[i].Speed.Z += dt * (startAcceleration.Z + crystalStruct.Atoms[i].Acceleration.Z) / (2.0 * new CrystalStruct.Mass.Mass().GetAtomMass(crystalStruct.Atoms[i].Type));
            }
        }
        private CrystalStruct.Coordinate.Coordinate GetAcceleration(CrystalStruct.CrystalStruct.CrystalStruct Struct, int index, bool getNeighbours)
        {
            //переменная для хранения значения потенциальной энергии при добавлении delta
            CrystalStruct.Coordinate.Coordinate up1 = new CrystalStruct.Coordinate.Coordinate();
            //переменная для хранения значения потенциальной энергии при вычитании delta
            CrystalStruct.Coordinate.Coordinate um1 = new CrystalStruct.Coordinate.Coordinate();
            //переменная для хранения значения потенциальной энергии при добавлении 2 * delta
            CrystalStruct.Coordinate.Coordinate up2 = new CrystalStruct.Coordinate.Coordinate();
            //переменная для хранения значения потенциальной энергии при вычитании 2 * delta
            CrystalStruct.Coordinate.Coordinate um2 = new CrystalStruct.Coordinate.Coordinate();

            //------------------------------------------------------------------------------------------

            CrystalStruct.Coordinate.Coordinate startCoordinate = new CrystalStruct.Coordinate.Coordinate
            {
                X = Struct.Atoms[index].Coordinate.X,
                Y = Struct.Atoms[index].Coordinate.Y,
                Z = Struct.Atoms[index].Coordinate.Z
            };

            //берем атом с номер - index в нашей структуре - struct, и прибавляем ему по OX delta
            Struct.Atoms[index].Coordinate.X = startCoordinate.X + Delta;
            //расчитываем энергию атома при добавлении delta
            up1.X = new Energy.Tersoff.Tersoff().AtomEnergy(Struct, index, getNeighbours);
            //берем атом с номер - index в нашей структуре - struct, и вычитаем ему по OX delta
            Struct.Atoms[index].Coordinate.X = startCoordinate.X - Delta;
            //расчитываем энергию атома при вычитании delta
            um1.X = new Energy.Tersoff.Tersoff().AtomEnergy(Struct, index, getNeighbours);
            //возвращаем значение координаты атома к начальному
            Struct.Atoms[index].Coordinate.X = startCoordinate.X;

            //берем атом с номер - index в нашей структуре - struct, и прибавляем ему по OY delta
            Struct.Atoms[index].Coordinate.Y = startCoordinate.Y + Delta;
            //расчитываем энергию атома при добавлении delta
            up1.Y = new Energy.Tersoff.Tersoff().AtomEnergy(Struct, index, getNeighbours);
            //берем атом с номер - index в нашей структуре - struct, и вычитаем ему по OY delta
            Struct.Atoms[index].Coordinate.Y = startCoordinate.Y - Delta;
            //расчитываем энергию атома при вычитании delta
            um1.Y = new Energy.Tersoff.Tersoff().AtomEnergy(Struct, index, getNeighbours);
            //возвращаем значение координаты атома к начальному
            Struct.Atoms[index].Coordinate.Y = startCoordinate.Y;

            //берем атом с номер - index в нашей структуре - struct, и прибавляем ему по OZ delta
            Struct.Atoms[index].Coordinate.Z = startCoordinate.Z + Delta;
            //расчитываем энергию атома при добавлении delta
            up1.Z = new Energy.Tersoff.Tersoff().AtomEnergy(Struct, index, getNeighbours);
            //берем атом с номер - index в нашей структуре - struct, и вычитаем ему по OX delta
            Struct.Atoms[index].Coordinate.Z = startCoordinate.Z - Delta;
            //расчитываем энергию атома при вычитании delta
            um1.Z = new Energy.Tersoff.Tersoff().AtomEnergy(Struct, index, getNeighbours);
            //возвращаем значение координаты атома к начальному
            Struct.Atoms[index].Coordinate.Z = startCoordinate.Z;

            //-----------------------------------------------------------------------------------------------------

            //берем атом с номер - index в нашей структуре - struct, и прибавляем ему по OX 2 *delta
            Struct.Atoms[index].Coordinate.X = startCoordinate.X + 2 * Delta;
            //расчитываем энергию атома при добавлении delta
            up2.X = new Energy.Tersoff.Tersoff().AtomEnergy(Struct, index, getNeighbours);
            //берем атом с номер - index в нашей структуре - struct, и вычитаем ему по OX 2 *delta
            Struct.Atoms[index].Coordinate.X = startCoordinate.X - 2 * Delta;
            //расчитываем энергию атома при вычитании delta
            um2.X = new Energy.Tersoff.Tersoff().AtomEnergy(Struct, index, getNeighbours);
            //возвращаем значение координаты атома к начальному
            Struct.Atoms[index].Coordinate.X = startCoordinate.X;

            //берем атом с номер - index в нашей структуре - struct, и прибавляем ему по OY 2 *delta
            Struct.Atoms[index].Coordinate.Y = startCoordinate.Y + 2 * Delta;
            //расчитываем энергию атома при добавлении delta
            up2.Y = new Energy.Tersoff.Tersoff().AtomEnergy(Struct, index, getNeighbours);
            //берем атом с номер - index в нашей структуре - struct, и вычитаем ему по OY 2 *delta
            Struct.Atoms[index].Coordinate.Y = startCoordinate.Y - 2 * Delta;
            //расчитываем энергию атома при вычитании delta
            um2.Y = new Energy.Tersoff.Tersoff().AtomEnergy(Struct, index, getNeighbours);
            //возвращаем значение координаты атома к начальному
            Struct.Atoms[index].Coordinate.Y = startCoordinate.Y;

            //берем атом с номер - index в нашей структуре - struct, и прибавляем ему по OZ 2 *delta
            Struct.Atoms[index].Coordinate.Z = startCoordinate.Z + 2 * Delta;
            //расчитываем энергию атома при добавлении delta
            up2.Z = new Energy.Tersoff.Tersoff().AtomEnergy(Struct, index, getNeighbours);
            //берем атом с номер - index в нашей структуре - struct, и вычитаем ему по OX delta
            Struct.Atoms[index].Coordinate.Z = startCoordinate.Z - 2 * Delta;
            //расчитываем энергию атома при вычитании delta
            um2.Z = new Energy.Tersoff.Tersoff().AtomEnergy(Struct, index, getNeighbours);
            //возвращаем значение координаты атома к начальному
            Struct.Atoms[index].Coordinate.Z = startCoordinate.Z;

            //---------------------------------------------------------------------------------------------------

            CrystalStruct.Coordinate.Coordinate acceleration = new CrystalStruct.Coordinate.Coordinate
            {
                X = -1.0 / (Delta * 12.0) * (8.0 * (up1.X - um1.X) + up2.X - um2.X),
                Y = -1.0 / (Delta * 12.0) * (8.0 * (up1.Y - um1.Y) + up2.Y - um2.Y),
                Z = -1.0 / (Delta * 12.0) * (8.0 * (up1.Z - um1.Z) + up2.Z - um2.Z)
            };
            //задаем координаты
            return acceleration;
        }
        private void NormalizationSpeeds(CrystalStruct.CrystalStruct.CrystalStruct crystalStruct, double kineticEquilibriumEnergy)
        {
            double beta = Math.Pow(kineticEquilibriumEnergy / ListEK[ListEK.Count - 1], 0.5);
            for (int j = 0; j < crystalStruct.Atoms.Count; j++)
            {
                crystalStruct.Atoms[j].Speed.X *= beta;
                crystalStruct.Atoms[j].Speed.Y *= beta;
                crystalStruct.Atoms[j].Speed.Z *= beta;
            }
        }

    }
}
