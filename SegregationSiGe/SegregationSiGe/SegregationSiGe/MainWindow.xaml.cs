﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Media3D;

namespace SegregationSiGe
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        CrystalStruct.CrystalStruct.CrystalStruct Struct = new CrystalStruct.CrystalStruct.CrystalStruct();
        Relaxation.Relaxation relaxation = new Relaxation.Relaxation(0.01);
        Visualization.Draw draw = new Visualization.Draw(60);
        ModelVisual3D model_visual = new ModelVisual3D();
        DistributionOfNeighbours.DistributionOfNeighbors DistributionOfNeighbours = new DistributionOfNeighbours.DistributionOfNeighbors();

        public MainWindow()
        {
            InitializeComponent();
        }
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            Data23GridRow.Height = new GridLength(0);
        }
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.NumPad8:
                    draw.CameraPhi += draw.CameraDPhi;
                    if (draw.CameraPhi > Math.PI / 2.0) draw.CameraPhi = Math.PI / 2.0;
                    draw.PositionCamera();
                    break;
                case Key.NumPad2:
                    draw.CameraPhi -= draw.CameraDPhi;
                    if (draw.CameraPhi < -Math.PI / 2.0) draw.CameraPhi = -Math.PI / 2.0;
                    draw.PositionCamera();
                    break;
                case Key.NumPad4:
                    draw.CameraTheta += draw.CameraDTheta;
                    draw.PositionCamera();
                    break;
                case Key.NumPad6:
                    draw.CameraTheta -= draw.CameraDTheta;
                    draw.PositionCamera();
                    break;
                case Key.NumPad1:
                    draw.CameraR -= draw.CameraDR;
                    // if (draw.CameraR < draw.CameraDR) draw.CameraR = draw.CameraDR;
                    draw.PositionCamera();
                    break;
                case Key.NumPad3:
                    draw.CameraR += draw.CameraDR;
                    draw.PositionCamera();
                    break;
            }
        }
        private void ShowView_Click(object sender, RoutedEventArgs e)
        {
            draw.DefineModel(Struct);
            model_visual.Content = draw.MainModel3Dgroup;
            MainViewport.Children.Clear();
            MainViewport.Children.Add(model_visual);
            MainViewport.Camera = draw.TheCamera;
        }

        private void SaveImage_Click(object sender, RoutedEventArgs e)
        {
            try
            {
               Visualization.Image.SaveToBmp(GridView, DateTime.Now.ToString("yyyy-dd-MM_hh-mm-ss") + ".bmp");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        private void Export_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string export = String.Empty;
                for (int i = 0; i < relaxation.ListEK.Count; i++)
                {
                    export += relaxation.ListEP[i].ToString() + ";" + relaxation.ListEK[i].ToString() + ";" + relaxation.ListTime[i].ToString() + ";\r\n";
                }
                File.WriteAllText("energy.csv", export);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }

        }

        private void Import_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Relaxation_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ShowEnergy.Visibility = Visibility.Visible;
                double timeStep = double.Parse(TimeStep.Text.ToString());
                int steps = int.Parse(Steps.Text.ToString());
                double temperature = double.Parse(Temperature.Text.ToString());

                relaxation.SetRelaxation(Struct, timeStep, steps, temperature, Normalization.IsChecked.Value);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }
        private void Create_Struct(object sender, RoutedEventArgs e)
        {
            try
            {
                relaxation = new Relaxation.Relaxation(0.01);
                //задаем концентрацию для Ge
                double concentrationGe = double.Parse(ConcentrationGe.Text.ToString());
                double paramCell = CrystalStruct.CellParameter.CellSiParameter * (1 - concentrationGe) + CrystalStruct.CellParameter.CellGeParameter * concentrationGe;
                int cellsCount = int.Parse(CellsCount.Text.ToString());

                Struct.CellParameter = paramCell;
                Struct.CrystallStructSize = new CrystalStruct.Coordinate.Coordinate { X = cellsCount, Y = cellsCount, Z = cellsCount };
                Struct.ConcentrationGe = concentrationGe;
                Struct.Create();

                if (SegregationCheck.IsChecked == true)
                {
                    int groupCount = int.Parse(GroupCountTextBox.Text.ToString());
                    Struct.SetAtomsType((int)CrystalStruct.Distribution.Distributions.Segregation, groupCount);
                }
                else
                {
                    Struct.SetAtomsType((int)CrystalStruct.Distribution.Distributions.Random);
                }
                MessageBox.Show("Структура создана");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }
        private void Check_Click(object sender, RoutedEventArgs e)
        {
            GroupCount.Visibility = Visibility.Visible;
            GroupCountTextBox.Visibility = Visibility.Visible;
        }
        private void Uncheck_Click(object sender, RoutedEventArgs e)
        {
            GroupCount.Visibility = Visibility.Hidden;
            GroupCountTextBox.Visibility = Visibility.Hidden;
        }
        private void Save_Click(object sender, RoutedEventArgs e)
        {
            var filename = DateTime.Now.ToString("hh_mm_ss");
            foreach (var item in relaxation.ListEP)
            {
                File.AppendAllText(filename, item.ToString());
            }

        }

        private void Permutation_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int count = int.Parse(CountPermutation.Text.ToString());
                double temperature = double.Parse(PermutationTemperature.Text.ToString());
                Console.Write(new Permutation.Permutation().SeveralPermutation(count, Struct, temperature));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        private void ShowEnergy_Click(object sender, RoutedEventArgs e)
        {
            if (ShowEnergy.Content.ToString() == "Hidden graphics")
            {
                Data23GridRow.Height = new GridLength(0);
                ShowEnergy.Content = "Show Ek(T) Ep(T)";
            }
            else
            {
                Data23GridRow.Height = new GridLength(300);
                List<Visualization.ToolkPoint> Chardata = new List<Visualization.ToolkPoint>();
                for (int i = 0; i < relaxation.ListEK.Count; i++)
                {
                    Chardata.Add(new Visualization.ToolkPoint { X = relaxation.ListTime[i], Y = relaxation.ListEK[i] });
                }
                ChartEk.ItemsSource = Chardata;
                Chardata = new List<Visualization.ToolkPoint>();
                for (int i = 0; i < relaxation.ListEP.Count; i++)
                {
                    Chardata.Add(new Visualization.ToolkPoint { X = relaxation.ListTime[i], Y = relaxation.ListEP[i] });
                }
                ChartEp.ItemsSource = Chardata;
                ShowEnergy.Content = "Hidden graphics";
            }
        }

        private void CalculateNeighbour_Click(object sender, RoutedEventArgs e)
        {
            DistributionOfNeighbours.GetAtomsNeighbour(Struct);
            DistributionOfNeighbours.WriteToFile("DistributionOfNeighbours.csv");
        }
    }
}
