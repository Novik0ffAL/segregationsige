﻿using System;
using System.Collections.Generic;

namespace SegregationSiGe.CrystalStruct.Cell
{
    public class Cell : IGetCellNeighbour, IGetCell
    {
        public Coordinate.Coordinate CellCoorfinate { get; set; }
        public List<Atom.Atom> Atoms { get; set; }
        public Dictionary<Coordinate.Coordinate, Cell> CellsNeighbours { get; set; }
        public Dictionary<Coordinate.Coordinate, Cell> GetCellNeighbour(CrystalStruct.CrystalStruct crystalStruct)
        {
            try
            {
                Dictionary<Coordinate.Coordinate, Cell> result = new Dictionary<Coordinate.Coordinate, Cell>();
                for (int i = 0; i < 3; i++)
                    for (int j = 0; j < 3; j++)
                        for (int k = 0; k < 3; k++)
                            result.Add(new Coordinate.Coordinate { X = 1 - i, Y = 1 - j, Z = 1 - k },
                                GetCell(crystalStruct, new Coordinate.Coordinate
                                {
                                    X = CellCoorfinate.X + 1 - i,
                                    Y = CellCoorfinate.Y + 1 - j,
                                    Z = CellCoorfinate.Z + 1 - k
                                }
                                ));

                return result;
            }
            catch (Exception)
            {
                throw new ArgumentException($@"error GetCellNeighbour");
            }
        }
        public Cell GetCell(CrystalStruct.CrystalStruct crystalStruct, Coordinate.Coordinate coordinate)
        {
            try
            {
                Coordinate.Coordinate temp = new Coordinate.Coordinate { X = coordinate.X, Y = coordinate.Y, Z = coordinate.Z };
                if (temp.X < 0) temp.X = crystalStruct.Cells.GetLength(0) - 1;
                if (temp.X > crystalStruct.Cells.GetLength(0) - 1) temp.X = 0;

                if (temp.Y < 0) temp.Y = crystalStruct.Cells.GetLength(1) - 1;
                if (temp.Y > crystalStruct.Cells.GetLength(1) - 1) temp.Y = 0;

                if (temp.Z < 0) temp.Z = crystalStruct.Cells.GetLength(2) - 1;
                if (temp.Z > crystalStruct.Cells.GetLength(2) - 1) temp.Z = 0;

                return crystalStruct.Cells[(int)temp.X, (int)temp.Y, (int)temp.Z];
            }
            catch (Exception)
            {
                throw new ArgumentException($@"error GetCell");
            }
        }
    }
}
