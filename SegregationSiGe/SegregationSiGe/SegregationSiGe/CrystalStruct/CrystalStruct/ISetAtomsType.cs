﻿namespace SegregationSiGe.CrystalStruct.CrystalStruct
{
    interface ISetAtomsType
    {
        void SetAtomsType(int distribution, int countGroup = 0);
    }
}
