﻿namespace SegregationSiGe.CrystalStruct.BondType
{
    interface IGetBondType
    {
        bool GetBondType(Atom.Atom a, Atom.Atom b, out int typeBond);
    }
}
