﻿namespace SegregationSiGe.CrystalStruct.Mass
{
    interface IGetAtomMass
    {
        double GetAtomMass(int type);
    }
}
