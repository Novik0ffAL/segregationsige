﻿using System;

namespace SegregationSiGe.CrystalStruct.Mass
{
    class Mass : IGetAtomMass
    {
        private const double MassGe  = 72.59;

        private const double MassSi = 28.0855;
        public double GetAtomMass(int type)
        {
            switch(type)
            {
                case 0: return MassGe;
                case 1: return MassSi;
                default: throw new ArgumentException("Неверный тип атома");
            }
        }
    }
}
