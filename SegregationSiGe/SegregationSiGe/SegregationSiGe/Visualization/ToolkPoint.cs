﻿namespace SegregationSiGe.Visualization
{
    public class ToolkPoint
    {
        public double X { get; set; }
        public double Y { get; set; }
    }
}
