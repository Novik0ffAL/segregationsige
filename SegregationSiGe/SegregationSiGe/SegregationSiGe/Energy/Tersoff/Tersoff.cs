﻿using System;
using System.Linq;

namespace SegregationSiGe.Energy.Tersoff
{
    public class Tersoff : IAtomEnergy, ITotalTersoffEnergy
    {
        public double A, B;
        public double LM, MU;
        public double BETTA;
        public double N, C, D, H;
        public double R, S;
        public double HI;
        public double W;

        public double TotalTersoffEnergy(CrystalStruct.CrystalStruct.CrystalStruct crystalStruct)
        {
            // переменная для подсчета всей энергии
            double totalEnergy = 0;
            //проходим все атомы в структруре
            for (int i = 0; i < crystalStruct.Atoms.Count(); i++)
            {
                totalEnergy += 0.5 * AtomEnergy(crystalStruct, i);
            }
            return totalEnergy;
        }
        public double AtomEnergy(CrystalStruct.CrystalStruct.CrystalStruct crystalStruct, int index, bool GetNeighbours = true)
        {
            if (GetNeighbours) { crystalStruct.Atoms[index].GetNeighbours(crystalStruct, crystalStruct.CellParameter / 2); }

            int J, K;//индексы атомов соседей
            double rij, rik, rjk;//расстояния
            double fcij, fcik;//FC(r)
            double frij, faij;//Fr(r),Fa(r)
            double dztij, cosQ;//Дзета, косинус угла между rij и rik
            double g, bij;

            double atomTotalEnergy = 0;

            for (int i = 0; i < crystalStruct.Atoms[index].Neighbours.Count; i++)
            {
                J = crystalStruct.Atoms[index].Neighbours.ElementAt(i).Key.Index;
                //тип связи между атом и атомом соседом - J
                CrystalStruct.BondType.IGetBondType getBondType = new CrystalStruct.BondType.BondType();
                getBondType.GetBondType(crystalStruct.Atoms[index], crystalStruct.Atoms[J], out int btij);
                //получаем расстояние между атомами J и Index
                rij = crystalStruct.Atoms[index].Coordinate.GetCoordinateDistance(new CrystalStruct.Coordinate.Coordinate
                {
                    X = crystalStruct.Atoms[index].Neighbours.ElementAt(i).Key.Coordinate.X +
                    crystalStruct.CellParameter * crystalStruct.Atoms[index].Neighbours.ElementAt(i).Value.X,

                    Y = crystalStruct.Atoms[index].Neighbours.ElementAt(i).Key.Coordinate.Y +
                    crystalStruct.CellParameter * crystalStruct.Atoms[index].Neighbours.ElementAt(i).Value.Y,

                    Z = crystalStruct.Atoms[index].Neighbours.ElementAt(i).Key.Coordinate.Z +
                    crystalStruct.CellParameter * crystalStruct.Atoms[index].Neighbours.ElementAt(i).Value.Z
                });
                //вычисляем FC(r)
                fcij = GetFC(rij, btij);
                if (Math.Abs(fcij) > 0e-5)
                {
                    //вычисляем Fr(r)
                    frij = Constants.Tersoffs[btij].A * Math.Exp(-Constants.Tersoffs[btij].LM * rij);
                    //вычисляем Fa(r)
                    faij = -Constants.Tersoffs[btij].B * Math.Exp(-Constants.Tersoffs[btij].MU * rij);
                    dztij = 0;
                    for (int j = 0; j < crystalStruct.Atoms[index].Neighbours.Count; j++)
                    {
                        K = crystalStruct.Atoms[index].Neighbours.ElementAt(j).Key.Index;
                        //условия что атомы соседи разные
                        if (J != K)
                        {
                            //тип связи между атом Index и атомом соседом - K
                            getBondType.GetBondType(crystalStruct.Atoms[index], crystalStruct.Atoms[K], out int btik);
                            //расстояние между Index и атомов соседом K
                            rik = crystalStruct.Atoms[index].Coordinate.GetCoordinateDistance(new CrystalStruct.Coordinate.Coordinate
                            {
                                X = crystalStruct.Atoms[index].Neighbours.ElementAt(j).Key.Coordinate.X +
                                    crystalStruct.CellParameter * crystalStruct.Atoms[index].Neighbours.ElementAt(j).Value.X,

                                Y = crystalStruct.Atoms[index].Neighbours.ElementAt(j).Key.Coordinate.Y +
                                    crystalStruct.CellParameter * crystalStruct.Atoms[index].Neighbours.ElementAt(j).Value.Y,

                                Z = crystalStruct.Atoms[index].Neighbours.ElementAt(j).Key.Coordinate.Z +
                                    crystalStruct.CellParameter * crystalStruct.Atoms[index].Neighbours.ElementAt(j).Value.Z
                            });
                            //вычисляем FC(r)
                            fcik = GetFC(rik, btik);
                            if (Math.Abs(fcik) > 0e-5)
                            {
                                // расстояние между атомами J, K
                                rjk = new CrystalStruct.Coordinate.Coordinate
                                {
                                    X = crystalStruct.Atoms[index].Neighbours.ElementAt(i).Key.Coordinate.X +
                                    crystalStruct.CellParameter * crystalStruct.Atoms[index].Neighbours.ElementAt(i).Value.X,

                                    Y = crystalStruct.Atoms[index].Neighbours.ElementAt(i).Key.Coordinate.Y +
                                    crystalStruct.CellParameter * crystalStruct.Atoms[index].Neighbours.ElementAt(i).Value.Y,

                                    Z = crystalStruct.Atoms[index].Neighbours.ElementAt(i).Key.Coordinate.Z +
                                    crystalStruct.CellParameter * crystalStruct.Atoms[index].Neighbours.ElementAt(i).Value.Z
                                }.GetCoordinateDistance(new CrystalStruct.Coordinate.Coordinate
                                {
                                    X = crystalStruct.Atoms[index].Neighbours.ElementAt(j).Key.Coordinate.X +
                                    crystalStruct.CellParameter * crystalStruct.Atoms[index].Neighbours.ElementAt(j).Value.X,

                                    Y = crystalStruct.Atoms[index].Neighbours.ElementAt(j).Key.Coordinate.Y +
                                    crystalStruct.CellParameter * crystalStruct.Atoms[index].Neighbours.ElementAt(j).Value.Y,

                                    Z = crystalStruct.Atoms[index].Neighbours.ElementAt(j).Key.Coordinate.Z +
                                    crystalStruct.CellParameter * crystalStruct.Atoms[index].Neighbours.ElementAt(j).Value.Z
                                });
                                cosQ = (rij * rij + rik * rik - rjk * rjk) / (2.0 * rij * rik);
                                // вычисление знаменателя в выражения для расчета g
                                double denominatorG = Math.Pow(Constants.Tersoffs[crystalStruct.Atoms[index].Type].H - cosQ, 2) + Math.Pow(Constants.Tersoffs[crystalStruct.Atoms[index].Type].D, 2);
                                // вычисление g
                                g = 1.0 + Math.Pow(Constants.Tersoffs[crystalStruct.Atoms[index].Type].C, 2) * (1.0 / Math.Pow(Constants.Tersoffs[crystalStruct.Atoms[index].Type].D, 2) - 1.0 / denominatorG);
                                dztij += Constants.Tersoffs[btik].W * fcik * g;
                            }
                            else return 0;
                        }
                    }
                    bij = Constants.Tersoffs[btij].HI * Math.Pow((1 + Math.Pow(Constants.Tersoffs[crystalStruct.Atoms[index].Type].BETTA * dztij, Constants.Tersoffs[crystalStruct.Atoms[index].Type].N)), -0.5 / Constants.Tersoffs[crystalStruct.Atoms[index].Type].N);
                    atomTotalEnergy += fcij * (frij + bij * faij);
                }
                else return 0;
            }
            if (atomTotalEnergy > 0) return 0;
            return atomTotalEnergy;
        }
        private static double GetFC(double R, int BT)
        {
            if (R <= Constants.Tersoffs[BT].R)
            {
                return 1.0;
            }
            else if (R > Constants.Tersoffs[BT].R && R < Constants.Tersoffs[BT].S)
            {
                return 0.5 + 0.5 * Math.Cos(Math.PI * (R - Constants.Tersoffs[BT].R) / (Constants.Tersoffs[BT].S - Constants.Tersoffs[BT].R));
            }
            else if (R >= Constants.Tersoffs[BT].S)
            {
                return 0.0;
            }
            return 0.0;
        }
    }
}
