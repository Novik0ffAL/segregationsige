﻿using System;

namespace SegregationSiGe.Energy
{
    public class Constants
    {
        public static readonly Tersoff.Tersoff[] Tersoffs = { new Tersoff.Tersoff(), new Tersoff.Tersoff(), new Tersoff.Tersoff() };
        public const double K = 8.617330350e-5;
        static Constants()
        {
            //ПАРАМЕТРЫ П-ЛА ТЕРСОФФА ДЛЯ SI
            Tersoffs[1].A = 1830.8;  //ЭВ
            Tersoffs[1].B = 471.18;//ЭВ
            Tersoffs[1].LM = 24.799;//1/НМ
            Tersoffs[1].MU = 17.322;//1/НМ
            Tersoffs[1].BETTA = 1.1E-6;
            Tersoffs[1].N = 0.78734;
            Tersoffs[1].C = 100390;
            Tersoffs[1].D = 16.217;
            Tersoffs[1].H = -0.59825;
            Tersoffs[1].R = 0.27;  //НМ
            Tersoffs[1].S = 0.3;  //НМ
            Tersoffs[1].HI = 1.0;
            Tersoffs[1].W = 1.0;
            //ПАРАМЕТРЫ П-ЛА ТЕРСОФФА ДЛЯ GE
            Tersoffs[0].A = 1769;         //ЭВ
            Tersoffs[0].B = 419.23;    //ЭВ
            Tersoffs[0].LM = 24.451;    //1/НМ
            Tersoffs[0].MU = 17.047;    //1/НМ
            Tersoffs[0].BETTA = 9.0166E-7;
            Tersoffs[0].N = 0.75627;
            Tersoffs[0].C = 106430;
            Tersoffs[0].D = 15.652;
            Tersoffs[0].H = -0.43884;
            Tersoffs[0].R = 0.28;    //НМ
            Tersoffs[0].S = 0.31;          //НМ
            Tersoffs[0].HI = 1.0;
            Tersoffs[0].W = 1;
            //ПАРАМЕТРЫ П-ЛА ТЕРСОФФА ДЛЯ SI-GE
            Tersoffs[2].A = Math.Sqrt(Tersoffs[0].A * Tersoffs[1].A);// ЭВ
            Tersoffs[2].B = Math.Sqrt(Tersoffs[0].B * Tersoffs[1].B);// ЭВ
            Tersoffs[2].LM = (Tersoffs[0].LM + Tersoffs[1].LM) * 0.5;//1 / НМ
            Tersoffs[2].MU = (Tersoffs[0].MU + Tersoffs[1].MU) * 0.5;//1 / НМ
            Tersoffs[2].R = Math.Sqrt(Tersoffs[0].R * Tersoffs[1].R); //НМ
            Tersoffs[2].S = Math.Sqrt(Tersoffs[0].S * Tersoffs[1].S); //НМ
            Tersoffs[2].HI = 1.00061;
            Tersoffs[2].W = 1.00;
        }
    }
}
