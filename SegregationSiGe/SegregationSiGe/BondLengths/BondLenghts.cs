﻿using SegregationSiGe.CrystalStruct.Atom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SegregationSiGe.BondLengths
{
    public class BondLenghts
    {
        Dictionary<string, TypeAndLenght> BondLenght { get; set; }
        public void SetBondLenght(CrystalStruct.CrystalStruct.CrystalStruct crystalStruct)
        {
            List<Atom> Same0Atom = new List<Atom>();
            List<Atom> Same1Atom = new List<Atom>();
            List<Atom> Same2Atom = new List<Atom>();
            List<Atom> Same3Atom = new List<Atom>();
            List<Atom> Same4Atom = new List<Atom>();

            foreach (var atom in crystalStruct.Atoms)
            {
                var count = (from neighbour in atom.Neighbours
                             where atom.Type.Equals(neighbour.Key.Type)
                             select neighbour).Count();
                switch (count)
                {
                    case 0: { Same0Atom.Add(atom); break; }
                    case 1: { Same1Atom.Add(atom); break; }
                    case 2: { Same2Atom.Add(atom); break; }
                    case 3: { Same3Atom.Add(atom); break; }
                    case 4: { Same4Atom.Add(atom); break; }
                }
            }
            int t = 0;
        }
    }
}
