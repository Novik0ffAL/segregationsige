﻿namespace SegregationSiGe.Permutation
{
    interface ISeveralPermutation
    {
        string SeveralPermutation(int count, CrystalStruct.CrystalStruct.CrystalStruct crystalStruct, double temperature = 0);
    }
}
