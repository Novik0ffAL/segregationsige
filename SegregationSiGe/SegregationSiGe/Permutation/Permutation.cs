﻿using System;

namespace SegregationSiGe.Permutation
{
    public class Permutation : ISeveralPermutation
    {
        public string SeveralPermutation(int count, CrystalStruct.CrystalStruct.CrystalStruct crystalStruct, double temperature = 0)
        {
            //количество успешных перестановок
            int countSuccess = 0;
            //считаем энергию до перестановок
            double beforeEnergy = new Energy.Tersoff.Tersoff().TotalTersoffEnergy(crystalStruct);
            Console.Write("\r\nЭнергия структуры до перестановок = " + beforeEnergy + "\r\n");
            //переменная для работы со случайными числами
            Random rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                bool check = true;
                while (check)
                {
                    //выбираем случайный атом в структуре
                    int a = Math.Abs(rnd.Next() % crystalStruct.Atoms.Count);
                    //выбираем случайного атома - соседа
                    int var = rnd.Next() % crystalStruct.Atoms[a].Neighbours.Count;
                    int index = 0;
                    int b = 0;
                    foreach (var item in crystalStruct.Atoms[a].Neighbours.Keys)
                    {
                        if (index == var) { b = item.Index; }
                        index++;
                    }

                    if (crystalStruct.Atoms[a].Type != crystalStruct.Atoms[b].Type)
                    {
                        if (TryPermutation(a, b, crystalStruct, temperature))
                        {
                            countSuccess++;
                        }
                        check = false;
                    }
                }
            }
            //считаем энергию после перестановок
            double afterEnergy = new Energy.Tersoff.Tersoff().TotalTersoffEnergy(crystalStruct);
            //возвращем разницу энергий после перестановок
            Console.Write("Энергия структуры после перестановок= " + afterEnergy + "\r\n");
            return "Разница энергий до перестановок и после  " + (afterEnergy - beforeEnergy) +
                    "\r\nКоличество перестановок  " + count +
                    "\r\nКоличество успешных перестановок  " + countSuccess;


        }
        private static bool TryPermutation(int a, int b, CrystalStruct.CrystalStruct.CrystalStruct crystalStruct, double temperature)
        {
            //делаем расчет энергии атомов a,b и соседей каждого до перестановки
            double beforeEnergy = new Energy.Tersoff.Tersoff().AtomEnergy(crystalStruct, a) + new Energy.Tersoff.Tersoff().AtomEnergy(crystalStruct, b);
            //переменная для получение случайного числа
            Random rnd = new Random();
            int temp = crystalStruct.Atoms[a].Type;
            //делаем перестановку
            crystalStruct.Atoms[a].Type = crystalStruct.Atoms[b].Type;
            crystalStruct.Atoms[b].Type = temp;
            //делаем расчет энергии атомов a,b и соседей каждого после перестановки
            double afterEnergy = new Energy.Tersoff.Tersoff().AtomEnergy(crystalStruct, a) + new Energy.Tersoff.Tersoff().AtomEnergy(crystalStruct, b);
            //если энергетически выгодна перестановка, то оставляем, а также оставляем перестановку если случайное число меньше вероятности задаваемой распределением Больцмана
            if (beforeEnergy >= afterEnergy ||
            temperature > 0 && Math.Exp(-Math.Abs(afterEnergy - beforeEnergy) / (temperature * Energy.Constants.K)) > rnd.NextDouble())
            {
                return true;
            }
            else
            {
                crystalStruct.Atoms[b].Type = crystalStruct.Atoms[a].Type;
                crystalStruct.Atoms[a].Type = temp;
                return false;
            }
        }

    }
}
