﻿namespace SegregationSiGe
{
    interface IWriteToFile
    {
        void WriteToFile(string path);
    }
}
