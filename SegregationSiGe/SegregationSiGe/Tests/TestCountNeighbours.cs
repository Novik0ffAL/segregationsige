﻿using System;

namespace SegregationSiGe.Tests
{
    public class TestCountNeighbours
    {
        static public void CheckCountsNeighbours(CrystalStruct.CrystalStruct.CrystalStruct crystalStruct)
        {
            Console.WriteLine("TestCountNeighbours");
            int tempCount = crystalStruct.Atoms.Count;
            foreach (var atom in crystalStruct.Atoms)
            {
                if (atom.Neighbours.Count != 4)
                {
                    tempCount--;
                    Console.Write($"Fail Atom - {atom.Index}");
                }
            }
            if (tempCount < crystalStruct.Atoms.Count)
            {
                Console.WriteLine($"Fail Test - Count Fails Atoms : {crystalStruct.Atoms.Count - tempCount}");
            }
            else { }
            Console.WriteLine($"Success Test");
        }
    }
}
