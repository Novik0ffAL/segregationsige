﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace SegregationSiGe.Visualization
{
    public class Draw
    {
        public Model3DGroup MainModel3Dgroup { get; } = new Model3DGroup();
        public PerspectiveCamera TheCamera { get; } = new PerspectiveCamera();
        public double CameraPhi = 0;
        public double CameraTheta = 0;
        public double CameraR = -20.0;
        public double CameraDPhi = 0.01;
        public double CameraDTheta = 0.01;
        public double CameraDR = 0.1;
        public int NumPhi = 10;
        public int NumTheta = 10;
        public SolidColorBrush brushGe = Brushes.Gray;
        public SolidColorBrush brushSi = Brushes.Yellow;
        public Draw(double FieldOfView)
        {
            TheCamera.FieldOfView = FieldOfView;
            PositionCamera();
        }
        public void DefineModel(CrystalStruct.CrystalStruct.CrystalStruct Struct)
        {
            //MainModel3Dgroup = new Model3DGroup();
            MainModel3Dgroup.Children.Clear();
            //задание источника света
            AmbientLight ambient_light = new AmbientLight(Colors.LightGray);
            MainModel3Dgroup.Children.Add(ambient_light);

            //добавление сфер(атомы) определенных цветов (brushGe,brushSi)
            for (int i = 0; i < Struct.Atoms.Count(); i++)
            {
                MeshGeometry3D mesh1 = new MeshGeometry3D();
                AddSmoothSphere(mesh1, new Point3D(Struct.Atoms[i].Coordinate.X - (Struct.Atoms[i].CellCoordinate.X - Struct.CrystallStructSize.X / 2 + 1) * Struct.CellParameter,
                    Struct.Atoms[i].Coordinate.Y - (Struct.Atoms[i].CellCoordinate.Y - Struct.CrystallStructSize.Y / 2 + 1) * Struct.CellParameter,
                    Struct.Atoms[i].Coordinate.Z - (Struct.Atoms[i].CellCoordinate.Z - Struct.CrystallStructSize.Z / 2 + 1) * Struct.CellParameter), 0.04, NumPhi, NumTheta);
                SolidColorBrush brush1 = brushGe;
                if (Struct.Atoms[i].Type == (int)CrystalStruct.AtomType.AtomTypes.Si)
                {
                    brush1 = brushSi;
                }
                DiffuseMaterial material1 = new DiffuseMaterial(brush1);
                GeometryModel3D model1 = new GeometryModel3D(mesh1, material1);
                //добавление полученной сферы
                MainModel3Dgroup.Children.Add(model1);
            }
        }
        private void AddSmoothTriangle(MeshGeometry3D mesh, Dictionary<Point3D, int> dict, Point3D point1, Point3D point2, Point3D point3)
        {
            int index1, index2, index3;

            // Find or create the points.
            if (dict.ContainsKey(point1)) index1 = dict[point1];
            else
            {
                index1 = mesh.Positions.Count;
                mesh.Positions.Add(point1);
                dict.Add(point1, index1);
            }

            if (dict.ContainsKey(point2)) index2 = dict[point2];
            else
            {
                index2 = mesh.Positions.Count;
                mesh.Positions.Add(point2);
                dict.Add(point2, index2);
            }

            if (dict.ContainsKey(point3)) index3 = dict[point3];
            else
            {
                index3 = mesh.Positions.Count;
                mesh.Positions.Add(point3);
                dict.Add(point3, index3);
            }

            // If two or more of the points are
            // the same, it's not a triangle.
            if ((index1 == index2) ||
                (index2 == index3) ||
                (index3 == index1)) return;

            // Create the triangle.
            mesh.TriangleIndices.Add(index1);
            mesh.TriangleIndices.Add(index2);
            mesh.TriangleIndices.Add(index3);
        }
        public void PositionCamera()
        {
            // Calculate the camera's position in Cartesian coordinates.
            double y = CameraR * Math.Sin(CameraPhi);
            double hyp = CameraR * Math.Cos(CameraPhi);
            double x = hyp * Math.Cos(CameraTheta);
            double z = hyp * Math.Sin(CameraTheta);
            TheCamera.Position = new Point3D(x, y, z);

            // Look toward the origin.
            TheCamera.LookDirection = new Vector3D(-x, -y, -z);

            // Set the Up direction.
            TheCamera.UpDirection = new Vector3D(0, 1, 0);
        }
        private void AddSmoothSphere(MeshGeometry3D mesh, Point3D center, double radius, int num_phi, int num_theta)
        {
            // Make a dictionary to track the sphere's points.
            Dictionary<Point3D, int> dict = new Dictionary<Point3D, int>();

            double phi0, theta0;
            double dphi = Math.PI / num_phi;
            double dtheta = 2 * Math.PI / num_theta;

            phi0 = 0;
            double y0 = radius * Math.Cos(phi0);
            double r0 = radius * Math.Sin(phi0);
            for (int i = 0; i < num_phi; i++)
            {
                double phi1 = phi0 + dphi;
                double y1 = radius * Math.Cos(phi1);
                double r1 = radius * Math.Sin(phi1);

                theta0 = 0;
                Point3D pt00 = new Point3D(
                    center.X + r0 * Math.Cos(theta0),
                    center.Y + y0,
                    center.Z + r0 * Math.Sin(theta0));
                Point3D pt10 = new Point3D(
                    center.X + r1 * Math.Cos(theta0),
                    center.Y + y1,
                    center.Z + r1 * Math.Sin(theta0));
                for (int j = 0; j < num_theta; j++)
                {
                    // Find the points with theta = theta1.
                    double theta1 = theta0 + dtheta;
                    Point3D pt01 = new Point3D(
                        center.X + r0 * Math.Cos(theta1),
                        center.Y + y0,
                        center.Z + r0 * Math.Sin(theta1));
                    Point3D pt11 = new Point3D(
                        center.X + r1 * Math.Cos(theta1),
                        center.Y + y1,
                        center.Z + r1 * Math.Sin(theta1));

                    // Create the triangles.
                    AddSmoothTriangle(mesh, dict, pt00, pt11, pt10);
                    AddSmoothTriangle(mesh, dict, pt00, pt01, pt11);

                    // Move to the next value of theta.
                    theta0 = theta1;
                    pt00 = pt01;
                    pt10 = pt11;
                }

                // Move to the next value of phi.
                phi0 = phi1;
                y0 = y1;
                r0 = r1;
            }
        }
    }
}
