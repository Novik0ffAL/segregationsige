﻿namespace SegregationSiGe.Energy.Tersoff
{
    interface IAtomEnergy
    {
        double AtomEnergy(CrystalStruct.CrystalStruct.CrystalStruct crystalStruct, int index, bool GetNeighbours = true);
    }
}
