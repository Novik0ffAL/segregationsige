﻿namespace SegregationSiGe.Energy.Tersoff
{
    interface ITotalTersoffEnergy
    {
        double TotalTersoffEnergy(CrystalStruct.CrystalStruct.CrystalStruct crystalStruct);
    }
}
