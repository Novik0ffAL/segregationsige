﻿namespace SegregationSiGe.Energy.KineticEnergy
{
    interface ITotalKineticEnergy
    {
        double TotalKineticEnergy(CrystalStruct.CrystalStruct.CrystalStruct crystalStruct);
    }
}
