﻿namespace SegregationSiGe.Energy.KineticEnergy
{
    public class KineticEnergy : IAtomKineticEnergy, ITotalKineticEnergy
    {
        public double AtomKineticEnergy(CrystalStruct.Atom.Atom atom)
        {
            return 0.5 * new CrystalStruct.Mass.Mass().GetAtomMass(atom.Type) * (atom.Speed.X * atom.Speed.X
                        + atom.Speed.Y * atom.Speed.Y
                        + atom.Speed.Z * atom.Speed.Z);
        }
        public double TotalKineticEnergy(CrystalStruct.CrystalStruct.CrystalStruct crystalStruct)
        {
            double totalKineticEnergy = 0;
            for (int i = 0; i < crystalStruct.Atoms.Count; i++)
            {
                totalKineticEnergy += AtomKineticEnergy(crystalStruct.Atoms[i]);
            }
            return totalKineticEnergy;
        }
    }
}
