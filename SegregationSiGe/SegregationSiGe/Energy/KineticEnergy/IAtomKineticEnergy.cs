﻿namespace SegregationSiGe.Energy.KineticEnergy
{
    interface IAtomKineticEnergy
    {
        double AtomKineticEnergy(CrystalStruct.Atom.Atom atom);
    }
}
